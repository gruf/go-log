module codeberg.org/gruf/go-log

go 1.21

require (
	codeberg.org/gruf/go-byteutil v1.2.0
	codeberg.org/gruf/go-kv v1.6.4
)
