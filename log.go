package log

import (
	"fmt"
	"io"
	"log"
	"sync/atomic"
	_ "unsafe"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
)

// stdflags are the log flags we use as a base for all Logger instances.
const stdflags = log.Ldate | log.Ltime

// Logger wraps the stdlib log.Logger to provided levelled logging.
type Logger struct {
	log *log.Logger // underlying standard lib logger
	lvl uint32      // currently set log level
	min uint8       // minimum call depth (used by global logger)
}

// New returns a new instance of Logger writing to given output. A
// minimum call depth may be specified to skip caller frames in caller info.
func New(w io.Writer, min uint8) *Logger {
	return NewFrom(log.New(w, "", stdflags), 1)
}

// NewFrom returns a new instance of Logger wrapping the provided standard library
// logger. A minimum call depth may be specified to skip caller frames in caller info.
func NewFrom(log *log.Logger, min uint8) *Logger {
	return &Logger{
		log: log,
		lvl: uint32(ALL),
		min: min,
	}
}

func (l *Logger) Debug(a ...interface{}) {
	l.Log(2, DEBUG, a...)
}

func (l *Logger) Debugf(s string, a ...interface{}) {
	l.Logf(2, DEBUG, s, a...)
}

func (l *Logger) DebugFields(fields ...kv.Field) {
	l.LogFields(2, DEBUG, fields...)
}

func (l *Logger) Info(a ...interface{}) {
	l.Log(2, INFO, a...)
}

func (l *Logger) Infof(s string, a ...interface{}) {
	l.Logf(2, INFO, s, a...)
}

func (l *Logger) InfoFields(fields ...kv.Field) {
	l.LogFields(2, INFO, fields...)
}

func (l *Logger) Warn(a ...interface{}) {
	l.Log(2, WARN, a...)
}

func (l *Logger) Warnf(s string, a ...interface{}) {
	l.Logf(2, WARN, s, a...)
}

func (l *Logger) WarnFields(fields ...kv.Field) {
	l.LogFields(2, WARN, fields...)
}

func (l *Logger) Error(a ...interface{}) {
	l.Log(2, ERROR, a...)
}

func (l *Logger) Errorf(s string, a ...interface{}) {
	l.Logf(2, ERROR, s, a...)
}

func (l *Logger) ErrorFields(fields ...kv.Field) {
	l.LogFields(2, ERROR, fields...)
}

func (l *Logger) Panic(a ...interface{}) {
	str := fmt.Sprint(a...)
	l.Log(2, PANIC, str)
	panic(str)
}

func (l *Logger) Panicf(s string, a ...interface{}) {
	str := fmt.Sprintf(s, a...)
	l.Log(2, PANIC, str)
	panic(str)
}

func (l *Logger) PanicFields(fields ...kv.Field) {
	str := kv.Fields(fields).String()
	l.Log(2, PANIC, str)
	panic(str)
}

// Log calls underlying stdlib log.Logger.Output() to print to the logger with level. Arguments are handled in the manner of fmt.Print.
func (l *Logger) Log(calldepth int, lvl LEVEL, a ...interface{}) {
	if lvl > l.Level() {
		return // no output
	}
	log_output(l.log, 0, int(l.min)+calldepth+1, func(b []byte) []byte {
		b = append(b, "["+levels[lvl]+"] "...)
		b = fmt.Append(b, a...)
		return b
	})
}

// Logf calls underlying stdlib log.Logger.Output() to print to the logger with level. Arguments are handled in the manner of fmt.Printf.
func (l *Logger) Logf(calldepth int, lvl LEVEL, s string, a ...interface{}) {
	if lvl > l.Level() {
		return // no output
	}
	log_output(l.log, 0, int(l.min)+calldepth+1, func(b []byte) []byte {
		b = append(b, "["+levels[lvl]+"] "...)
		b = fmt.Appendf(b, s, a...)
		return b
	})
}

// Logf calls underlying stdlib log.Logger.Output() to print to the logger with level. Arguments formatted using fv.Fields{}.String().
func (l *Logger) LogFields(calldepth int, lvl LEVEL, fields ...kv.Field) {
	if lvl > l.Level() {
		return // no output
	}
	log_output(l.log, 0, int(l.min)+calldepth+1, func(b []byte) []byte {
		buf := byteutil.Buffer{B: b}
		buf.B = append(buf.B, "["+levels[lvl]+"] "...)
		kv.Fields(fields).AppendFormat(&buf, false)
		return buf.B
	})
}

// Print calls underlying stdlib log.Logger.Output() to print to the logger. Arguments are handled in the manner of fmt.Print.
func (l *Logger) Print(a ...interface{}) {
	log_output(l.log, 0, int(l.min)+2, func(b []byte) []byte {
		return fmt.Append(b, a...)
	})
}

// Printf calls underlying stdlib log.Logger.Output() to print to the logger. Arguments are handled in the manner of fmt.Printf.
func (l *Logger) Printf(s string, a ...interface{}) {
	log_output(l.log, 0, int(l.min)+2, func(b []byte) []byte {
		return fmt.Appendf(b, s, a...)
	})
}

// PrintFields calls underlying stdlib log.Logger.Output() to print to the logger. Arguments formatted using fv.Fields{}.String().
func (l *Logger) PrintFields(fields ...kv.Field) {
	log_output(l.log, 0, int(l.min)+2, func(b []byte) []byte {
		buf := byteutil.Buffer{B: b}
		kv.Fields(fields).AppendFormat(&buf, false)
		return buf.B
	})
}

// Level returns the minimum logging level for the logger.
func (l *Logger) Level() LEVEL {
	return LEVEL(atomic.LoadUint32(&l.lvl))
}

// SetLevel sets the minimum logging level for the logger.
func (l *Logger) SetLevel(lvl LEVEL) {
	atomic.StoreUint32(&l.lvl, uint32(lvl))
}

// IncludeCaller returns whether 'IncludeCaller' is enabled.
func (l *Logger) IncludeCaller() bool {
	return l.log.Flags()&log.Llongfile != 0
}

// SetIncludeCaller enables/disbables logging with added caller information.
func (l *Logger) SetIncludeCaller(enabled bool) {
	flags := stdflags
	if enabled {
		flags |= log.Llongfile
	}
	l.log.SetFlags(flags)
}

// SetOutput sets the output destination for the logger.
func (l *Logger) SetOutput(out io.Writer) {
	l.log.SetOutput(out)
}

// Logger returns the underlying stdlib log.Logger.
func (l *Logger) Logger() *log.Logger {
	return l.log
}

//go:linkname log_output log.(*Logger).output
func log_output(l *log.Logger, pc uintptr, calldepth int, appendOutput func([]byte) []byte) error
