package log

import (
	"io"
	"log"

	"codeberg.org/gruf/go-kv"
)

func init() {
	// Init global + set default flags
	global = NewFrom(log.Default(), 1)
	global.Logger().SetFlags(stdflags)
}

// global is the global Logger instance built using log's default.
var global *Logger

// Default returns the default Logger instance.
func Default() *Logger { return global }

/* below are direct call-throughs to global Logger. */

func Debug(a ...interface{})                                    { global.Debug(a...) }
func Debugf(s string, a ...interface{})                         { global.Debugf(s, a...) }
func DebugFields(fields ...kv.Field)                            { global.DebugFields(fields...) }
func Info(a ...interface{})                                     { global.Info(a...) }
func Infof(s string, a ...interface{})                          { global.Infof(s, a...) }
func InfoFields(fields ...kv.Field)                             { global.InfoFields(fields...) }
func Warn(a ...interface{})                                     { global.Warn(a...) }
func Warnf(s string, a ...interface{})                          { global.Warnf(s, a...) }
func WarnFields(fields ...kv.Field)                             { global.WarnFields(fields...) }
func Error(a ...interface{})                                    { global.Error(a...) }
func Errorf(s string, a ...interface{})                         { global.Errorf(s, a...) }
func ErrorFields(fields ...kv.Field)                            { global.ErrorFields(fields...) }
func Panic(a ...interface{})                                    { global.Panic(a...) }
func Panicf(s string, a ...interface{})                         { global.Panicf(s, a...) }
func PanicFields(fields ...kv.Field)                            { global.PanicFields(fields...) }
func Log(calldepth int, lvl LEVEL, a ...interface{})            { global.Log(calldepth, lvl, a...) }
func Logf(calldepth int, lvl LEVEL, s string, a ...interface{}) { global.Logf(calldepth, lvl, s, a...) }
func LogFields(calldepth int, lvl LEVEL, fields ...kv.Field) {
	global.LogFields(calldepth, lvl, fields...)
}
func Print(a ...interface{})            { global.Print(a...) }
func Printf(s string, a ...interface{}) { global.Printf(s, a...) }
func PrintFields(fields ...kv.Field)    { global.PrintFields(fields...) }
func Level() LEVEL                      { return global.Level() }
func SetLevel(lvl LEVEL)                { global.SetLevel(lvl) }
func IncludeCaller() bool               { return global.IncludeCaller() }
func SetIncludeCaller(verbose bool)     { global.SetIncludeCaller(verbose) }
func SetOutput(out io.Writer)           { global.SetOutput(out) }
