package log

// LEVEL represents a logging level (e.g. info, warn, ...).
type LEVEL uint8

const (
	// Available log levels.
	NONE  LEVEL = 0
	PANIC LEVEL = 50
	ERROR LEVEL = 100
	WARN  LEVEL = 150
	INFO  LEVEL = 200
	DEBUG LEVEL = 250
	ALL   LEVEL = ^LEVEL(0)
)

// defaultLevels is a lookup table of default log levels.
var levels = [int(ALL) + 1]string{
	PANIC: "PANIC",
	ERROR: "ERROR",
	WARN:  "WARN",
	INFO:  "INFO",
	DEBUG: "DEBUG",
}
