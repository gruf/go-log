package log_test

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"testing"

	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-log"
)

var (
	testArgs = [][]interface{}{
		{0, 1, 2, 3, 4},
		{"hello", "world!"},
		{"random", 0, 1, 2.2, 3.3, "arguments"},
		{[]int{0, 1, 2, 3}, []string{"hello", "world"}, map[string]string{"hello": "world"}},
		{http.Client{}, sync.Mutex{}},
		{nil, nil, nil, nil},
	}

	testFmts = []struct {
		fmt string
		arg []interface{}
	}{
		{
			fmt: "hello world: %q",
			arg: []interface{}{"hello world"},
		},
		{
			fmt: "no operators here",
			arg: []interface{}{"oh no unexpected!"},
		},
		{
			fmt: "0, 1, %d, %f, %s",
			arg: []interface{}{2, 3.0, "4"},
		},
		{
			fmt: "%d %d %d %d",
			arg: []interface{}{"oh no bad and missing operators"},
		},
		{
			fmt: "%#v",
			arg: []interface{}{http.Client{}},
		},
		{
			fmt: "%+v",
			arg: []interface{}{map[string]interface{}{"key": "value", "hello": "world"}},
		},
	}

	testFields = []kv.Fields{
		{
			kv.Field{K: "hello", V: "world!"},
			kv.Field{K: "client", V: http.Client{}},
		},
		{
			kv.Field{K: "map", V: map[string]string{"map": "of values"}},
			kv.Field{K: "slice", V: []string{"slice", "of", "values"}},
		},
		{
			kv.Field{K: "nil", V: nil},
			kv.Field{K: "hmmm", V: "watcha sayyyy"},
		},
		{
			kv.Field{K: "int", V: 420},
			kv.Field{K: "float", V: 6.9},
		},
		{
			kv.Field{K: "complex", V: complex(120, 120)},
			kv.Field{K: "shrug", V: "\n\n\n\n\n\n\n\n\n\n\n\n"},
		},
	}
)

func TestLoggingLevels(t *testing.T) {
	// Special case of no log flags
	log.Default().Logger().SetFlags(0)

	// Output to buffer
	buf := bytes.Buffer{}
	log.SetOutput(&buf)

	testLog := func(do func(), expect bool) {
		do()
		if !expect && buf.Len() > 0 {
			t.Error("log should not have produced output")
		} else if expect && buf.Len() == 0 {
			t.Error("log should have produced output")
		}
		buf.Reset()
	}

	for _, lvl := range []log.LEVEL{
		log.ALL,
		log.DEBUG,
		log.INFO,
		log.WARN,
		log.ERROR,
		log.PANIC,
		log.NONE,
	} {
		// Update log level
		log.SetLevel(lvl)

		// DEBUG level
		testLog(func() {
			log.Debug("test log")
		}, lvl >= log.DEBUG)
		testLog(func() {
			log.Debugf("test log")
		}, lvl >= log.DEBUG)
		testLog(func() {
			log.DebugFields(kv.Field{K: "test", V: "log"})
		}, lvl >= log.DEBUG)

		// INFO level
		testLog(func() {
			log.Info("test log")
		}, lvl >= log.INFO)
		testLog(func() {
			log.Infof("test log")
		}, lvl >= log.INFO)
		testLog(func() {
			log.InfoFields(kv.Field{K: "test", V: "log"})
		}, lvl >= log.INFO)

		// WARN level
		testLog(func() {
			log.Warn("test log")
		}, lvl >= log.WARN)
		testLog(func() {
			log.Warnf("test log")
		}, lvl >= log.WARN)
		testLog(func() {
			log.WarnFields(kv.Field{K: "test", V: "log"})
		}, lvl >= log.WARN)

		// ERROR level
		testLog(func() {
			log.Error("test log")
		}, lvl >= log.ERROR)
		testLog(func() {
			log.Errorf("test log")
		}, lvl >= log.ERROR)
		testLog(func() {
			log.ErrorFields(kv.Field{K: "test", V: "log"})
		}, lvl >= log.ERROR)

		// PANIC level
		testLog(func() {
			defer func() { _ = recover() }()
			log.Panic("test log")
		}, lvl >= log.PANIC)
		testLog(func() {
			defer func() { _ = recover() }()
			log.Panicf("test log")
		}, lvl >= log.PANIC)
		testLog(func() {
			defer func() { _ = recover() }()
			log.PanicFields(kv.Field{K: "test", V: "log"})
		}, lvl >= log.PANIC)
	}
}

func TestLoggerOutput(t *testing.T) {
	// Special case of no log flags
	log.Default().Logger().SetFlags(0)

	// Output to buffer
	buf := bytes.Buffer{}
	log.SetOutput(&buf)

	// Reset log level
	log.SetLevel(log.ALL)

	testLog := func(do func(), match func(string) bool) {
		do()
		if str := buf.String(); !match(strings.TrimSpace(str)) {
			t.Errorf("unexpected log output: %s", str)
		}
		buf.Reset()
	}

	for _, arg := range testArgs {
		expect := fmt.Sprint(arg...)

		testLog(func() {
			log.Debug(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[DEBUG] ") &&
				(strings.TrimPrefix(out, "[DEBUG] ") == expect)
		})

		testLog(func() {
			log.Info(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[INFO] ") &&
				(strings.TrimPrefix(out, "[INFO] ") == expect)
		})

		testLog(func() {
			log.Warn(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[WARN] ") &&
				(strings.TrimPrefix(out, "[WARN] ") == expect)
		})

		testLog(func() {
			log.Error(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[ERROR] ") &&
				(strings.TrimPrefix(out, "[ERROR] ") == expect)
		})

		testLog(func() {
			defer func() {
				if recover() != expect {
					t.Error("unexpected panic result")
				}
			}()
			log.Panic(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[PANIC] ") &&
				(strings.TrimPrefix(out, "[PANIC] ") == expect)
		})
	}

	for _, format := range testFmts {
		expect := fmt.Sprintf(format.fmt, format.arg...)

		testLog(func() {
			log.Debugf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[DEBUG] ") &&
				(strings.TrimPrefix(out, "[DEBUG] ") == expect)
		})

		testLog(func() {
			log.Infof(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[INFO] ") &&
				(strings.TrimPrefix(out, "[INFO] ") == expect)
		})

		testLog(func() {
			log.Warnf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[WARN] ") &&
				(strings.TrimPrefix(out, "[WARN] ") == expect)
		})

		testLog(func() {
			log.Errorf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[ERROR] ") &&
				(strings.TrimPrefix(out, "[ERROR] ") == expect)
		})

		testLog(func() {
			defer func() {
				if recover() != expect {
					t.Error("unexpected panic result")
				}
			}()
			log.Panicf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[PANIC] ") &&
				(strings.TrimPrefix(out, "[PANIC] ") == expect)
		})
	}

	for _, fields := range testFields {
		expect := fields.String()

		testLog(func() {
			log.DebugFields(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[DEBUG] ") &&
				(strings.TrimPrefix(out, "[DEBUG] ") == expect)
		})

		testLog(func() {
			log.InfoFields(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[INFO] ") &&
				(strings.TrimPrefix(out, "[INFO] ") == expect)
		})

		testLog(func() {
			log.WarnFields(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[WARN] ") &&
				(strings.TrimPrefix(out, "[WARN] ") == expect)
		})

		testLog(func() {
			log.ErrorFields(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[ERROR] ") &&
				(strings.TrimPrefix(out, "[ERROR] ") == expect)
		})

		testLog(func() {
			defer func() {
				if recover() != expect {
					t.Error("unexpected panic result")
				}
			}()
			log.PanicFields(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "[PANIC] ") &&
				(strings.TrimPrefix(out, "[PANIC] ") == expect)
		})
	}
}
